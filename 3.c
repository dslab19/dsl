#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define push(c) stack[++top] = c
#define pop() stack[top--]
#define peek() stack[top]
#define isempty() (top < 0)
#define safepop() if(!isempty()) pop()

int isop(char c){
    if(c == '*' || c == '/' || c == '+' || c == '-' || c == '^' || c == '$') return 1;
    return 0;

}

int prec(char input)
{
    switch (input) {
    case '+':
    case '-':
        return 2;
    case '*':
    case '%':
    case '/':
        return 4;
    case '^':
        return 5;
    case '(':
    case '$':
        return 0;
    }
}

int main() {
    char expression[250] = "a+b*(c^d-e)^(f+g*h)-i";
    char stack[25];
    int top = 0, i = 0;
    while(expression[i] != '\0'){
        char curr = expression[i++];
        if(isalnum(curr)){
            printf("%c", curr);
        }else if(curr == '('){
            push('(');
        }else if(curr == ')'){
            while(!isempty() && peek() != '('){
                printf("%c", pop());
            }
            if(peek() == '('){
                safepop();
            }
        }else if(isop(curr)){
            while(!isempty() && prec(curr) <= prec(peek())){
                printf("%c", pop());
            }
            push(curr);
        }
    }
    while(!isempty()){
            printf("%c", pop());
    }
    return 0;
}
