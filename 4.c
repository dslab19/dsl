#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define push(c) stack[++top] = c
#define pop() stack[top--]
#define peek() stack[top]
#define isempty() (top < 0)
#define safepop() if(!isempty()) pop()

int isop(char c){
    if(c == '*' || c == '/' || c == '+' || c == '-' || c == '^' || c == '$') return 1;
    return 0;

}

int main() {
    char expression[250];
    printf("Enter expression :: ");
    scanf("%s", expression);
    int stack[25];
    int top = 0, i = 0;
    while(expression[i] != '\0'){
        char curr = expression[i++];
        if(isalnum(curr)){
            int val = curr - '0';
            push(val);
        }else if(isop(curr)){
            int a = pop();
            int b = pop();
            int res;
            switch(curr){
                case '+':
                    res = b + a;
                    break;
                case '-':
                    res = b - a;
                    break;
                case '*':
                    res = b * a;
                    break;
                case '/':
                    res = b / a;
                    break;
                case '%':
                    res = b % a;
                    break;
                case '^':
                    res = b ^ a;
                    break;
            }
            push(res);
        }
    }
    if(!isempty()){
        printf("%d", pop());
    }
    return 0;
}