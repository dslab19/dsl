#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Node
{
	int data;
	struct Node *next;
} Node;

Node *newNode(int key, Node *nxt)
{
	Node *temp = (Node *)malloc(sizeof(Node));
	temp->data = key;
	temp->next = nxt;
	return temp;
}

void printList(Node *node)
{
	while (node != NULL)
	{
		printf("%d ", node->data);
		node = node->next;
	}
}

Node *recursive_ordered_insert(Node *node, int value)
{
	if (node != NULL && node->data == value)
		return node;

	if ((node == NULL) || !(node->data < value))
	{
		return newNode(value, node);
	}
	node->next = recursive_ordered_insert(node->next, value);

	return node;
}

Node *merge(Node *h1, Node *h2)
{
	if (!h1)
		return h2;
	if (!h2)
		return h1;

	if (h1->data < h2->data)
	{
		h1->next = merge(h1->next, h2);
		return h1;
	}
	else
	{
		h2->next = merge(h1, h2->next);
		return h2;
	}
}

Node *read(Node *node)
{
	int data;
	char line[255];
	printf("Enter value :: ");
	scanf("%s", line);
	while (strcmpi(line, "end") != 0)
	{
		sscanf(line, "%d", &data);
		node = recursive_ordered_insert(node, data);
		printf("Enter value :: ");
		scanf("%s", line);
	}
	return node;
}

int main()
{
	Node *head1 = NULL;
	printf("Enter list 1 values\n");
	head1 = read(head1);
	Node *head2 = NULL;
	printf("Enter list 2 values\n");
	head2 = read(head2);
	Node *mergedhead = merge(head1, head2);
	printf("Merged list \n");
	printList(mergedhead);
	return 0;
}
