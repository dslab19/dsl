#include<stdio.h>
#include<stdlib.h>
#include<ctype.h>
#include<math.h>

typedef struct node
{
    int d;
    struct node *l, *r;
} tree;

tree *create_tree(char pf[])
{
    tree *newnode, *stack[20];
    int i = 0, top = -1;
    char ch;
    while ((ch = pf[i++]) != '\0')
    {
        newnode = (tree *)malloc(sizeof(tree));
        newnode->d = ch;
        newnode->l = newnode->r = NULL;
        if (isalnum(ch))
            stack[++top] = newnode;
        else
        {
            newnode->r = stack[top--];
            newnode->l = stack[top--];
            stack[++top] = newnode;
        }
    }
    return (stack[top--]);
}

float eval(tree *root)
{
    float num;
    switch (root->d)
    {
    case '+':
        return (eval(root->l) + eval(root->r));
    case '-':
        return (eval(root->l) - eval(root->r));
    case '*':
        return (eval(root->l) * eval(root->r));
    case '/':
        return (eval(root->l) / eval(root->r));
    case '^':
        return (pow(eval(root->l), eval(root->r)));
    default:
        if (isalpha(root->d))
        {
            printf("\n%c = ", root->d);
            scanf("%f", &num);
            return (num);
        }
        else
            return (root->d - '0');
    }
}

int main()
{
    char postfix[30];
    float res;
    tree *root = NULL;
    printf("\nEnter a valid Postfix expression\n");
    scanf("%s", postfix);
    root = create_tree(postfix);
    res = eval(root);
    printf("\nResult = %f", res);
    return 0;
}
