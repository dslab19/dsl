#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define max 25
#define isfull() (front == max)
#define isempty() (front == rear)
#define deque()   arr[front++]
#define queue(c)  arr[rear++] = c
#define newline() printf("\n");
#define display() newline();\
                  for(i = front; i< rear; i++)\
                  {\
                     printf("%d, ", arr[i]);\
                  }\
                  newline();

#define msg0 "1. add to queue\n2. remove from queue\n3. display\n4. print this message\n5. exit"
#define msg1 "Enter value to add to queue:: "


int read(){
    int temp;
    scanf("%d", &temp);
    return temp;
}

int main() {
    int arr[max];
    int front = 0; int rear = 0;
    int op, i;
    printf(msg0);
    newline();
    while(1){
        scanf("%d", &op);
        switch(op){
                case 1:
                    printf(msg1);
                    queue(read());
                    break;
                case 2:
                    printf("\ndequeued element -> %d\n", deque());
                    break;
                case 3:
                    display();
                    break;
                case 4:
                    printf(msg0);
                    newline();
                    break;
                case 5:
                    return 1;
        }
    }
    return 0;
}
