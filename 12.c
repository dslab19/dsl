#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct node
{
    struct node *l, *r;
    int d;
} tree;

tree *newNode(int data, tree *nxt)
{
    tree *new = (tree *)malloc(sizeof(tree));
    new->d = data;
    new->l = new->r = NULL;
    return new;
}


tree* insert(tree *root, tree *data)
{
    if (root == NULL)
    {
        root = data;
        return root;
    }

    if (root->d < data->d)
    {
        tree * r = insert(root->r, data);
        root->r = r;
    }
    else if (root->d > data->d)
    {
        tree * l = insert(root->l, data);
        root->l = l;
    }
    return root;
}

tree* minValueNode(tree* node)
{
    tree* current = node;

    while (current && current->l != NULL)
        current = current->l;

    return current;
}

tree* deleteNode(tree* root, int key)
{
    if (root == NULL) return root;

    if (key < root->d)
        root->l = deleteNode(root->l, key);

    else if (key > root->d)
        root->r = deleteNode(root->r, key);

    else
    {
        if (root->l == NULL)
        {
            struct node *temp = root->r;
            free(root);
            return temp;
        }
        else if (root->r == NULL)
        {
            struct node *temp = root->l;
            free(root);
            return temp;
        }

        tree *temp = minValueNode(root->r);
        root->d = temp->d;
        root->r = deleteNode(root->r, temp->d);
    }
    return root;
}

void inorder(tree *root)
{
    if (root == NULL)
        return;
    inorder(root->l);
    printf("%d; ", root->d);
    inorder(root->r);
}

tree * read(tree *mtree)
{
    int data;
    char line[255];
    printf("Enter value :: ");
    scanf("%s", line);
    while (strcmpi(line, "end") != 0)
    {
        sscanf(line, "%d", &data);
        mtree = insert(mtree, newNode(data, NULL));
        printf("Enter value :: ");
        scanf("%s", line);
    }
    return mtree;
}

int main()
{
    tree *myTree = NULL;
    myTree = read(myTree);
    int code, data;
    while(1){
        printf("\nEnter operation code :: ");
        scanf("%d", &code);
        if(code == 1){
            inorder(myTree);
        }else if(code == 2){
            printf("\nEnter data to delete :: ");
            scanf("%d", &data);
            myTree = deleteNode(myTree, data);

        }else{
            break;
        }
    }
    return 0;
}