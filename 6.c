#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define max 5
#define isfull() ((front == 0 && rear == max-1) || (rear == (front-1)%(max-1)))
#define isempty() (front == -1)
#define newline() printf("\n");

#define msg0 "1. add to queue\n2. remove from queue\n3. display\n4. print this message\n5. exit"
#define msg1 "Enter value to add to queue:: "


int front = -1; int rear = -1;
int arr[max];

int read(){
    int temp;
    scanf("%d", &temp);
    return temp;
}

void queue(int value)
{
    if (isfull())
    {
        printf("\nQueue is Full");
        return;
    }

    else if (front == -1)
    {
        front = rear = 0;
        arr[rear] = value;
    }

    else if (rear == max-1 && front != 0)
    {
        rear = 0;
        arr[rear] = value;
    }

    else
    {
        rear++;
        arr[rear] = value;
    }
}

int dequeue()
{
    if (isempty())
    {
        printf("\nQueue is Empty");
        return INT_MIN;
    }

    int data = arr[front];
    arr[front] = -1;
    if (front == rear)
    {
        front = -1;
        rear = -1;
    }
    else if (front == max-1)
        front = 0;
    else
        front++;

    return data;
}

void display()
{
    if (isempty())
    {
        printf("\nQueue is Empty");
        return;
    }
    printf("\nElements in Circular Queue are: ");
    if (rear >= front)
    {
        for (int i = front; i <= rear; i++)
            printf("%d ",arr[i]);
    }
    else
    {
        for (int i = front; i < max; i++)
            printf("%d ", arr[i]);

        for (int i = 0; i <= rear; i++)
            printf("%d ", arr[i]);
    }
}

int main() {
    int op, i;
    printf(msg0);
    newline();
    while(1){
        scanf("%d", &op);
        switch(op){
                case 1:
                    printf(msg1);
                    queue(read());
                    break;
                case 2:
                    printf("\ndequeued element -> %d\n", dequeue());
                    break;
                case 3:
                    display();
                    break;
                case 4:
                    printf(msg0);
                    newline();
                    break;
                case 5:
                    return 1;
        }
    }
    return 0;
}
